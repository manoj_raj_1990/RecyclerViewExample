package com.manoj.recyclerview.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.manoj.recyclerview.R;
import com.manoj.recyclerview.model.DataModel;
import com.manoj.recyclerview.model.pojo.ListData;
import com.manoj.recyclerview.view.adapter.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @OnClick(R.id.linear_list)
    void linearData(){
        setLayoutManager(1);
    }

    @OnClick(R.id.horizontal_list)
    void horizontalData(){
        setLayoutManager(2);
    }

    @OnClick(R.id.add_data)
    void addNewData(){
        dataModel.addNewData("List Name "+dataList.size());
        recyclerView.scrollToPosition(dataList.size());
    }

    List<ListData> dataList = new ArrayList<>();
    RecyclerAdapter recyclerAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setLayoutManager(1);

        dataModel.listData.observe(this, new Observer<List<ListData>>() {
            @Override
            public void onChanged(@Nullable List<ListData> listData) {
                dataList.addAll(listData);
                notifyAdapter();
            }
        });
    }

    private void lodeData() {
        dataList.clear();
        dataList.addAll(dataModel.getData());
        notifyAdapter();
    }

    private void notifyAdapter() {
        recyclerAdapter.notifyDataSetChanged();
    }

    /**
     * Case 1: Linear List
     * Case 2: Horizontal List
     * */
    private void setLayoutManager(int i) {
        switch (i){
            case 1:
                setLinearLayout(recyclerView);
                break;
            case 2:
                setHorizontalLayout(recyclerView);
                break;
        }
        recyclerAdapter = new RecyclerAdapter(this, dataList,i);
        recyclerView.setAdapter(recyclerAdapter);
    }

}