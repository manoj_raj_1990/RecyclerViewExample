package com.manoj.recyclerview.view;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.manoj.recyclerview.model.DataModel;

import butterknife.ButterKnife;

/**
 * Copyright (c) Manoj
 *
 * Created by manoj on 09-02-2018.
 *
 * It's the common base extend class for all activity that
 * we create in this project.
 *
 * Define all the variables and method that we need it as a global
 * access for different activity.
 *
 * @BaseActivity extends @PermissionActivity all the runtime
 * permission are defined and declared in it. If we just want
 * to check or request the permission. requestPermissionAction()
 * method.
 *
 * Both @BaseActivity & @PermissionActivity are abstract so it's not
 * mandatory to Override all the abstract methods for Extending
 * Class @PermissionActivity. Hence @BaseActivity is not a Concert Class.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract int getLayoutResource();
    public DataModel dataModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);
        dataModel = ViewModelProviders.of(this).get(DataModel.class);
    }

    public void setLinearLayout(RecyclerView recyclerView){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setHorizontalLayout(RecyclerView recyclerView){
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

}