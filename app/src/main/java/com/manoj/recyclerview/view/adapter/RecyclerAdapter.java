package com.manoj.recyclerview.view.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.manoj.recyclerview.R;
import com.manoj.recyclerview.model.pojo.ListData;
import com.manoj.recyclerview.view.MainActivity;
import com.manoj.recyclerview.view.adapter.common.CommonRecyclerAdapter;
import com.manoj.recyclerview.view.adapter.common.CommonViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends CommonRecyclerAdapter<CommonViewHolder> {

    Integer type;
    Activity activity;
    List<ListData> dataList = new ArrayList<>();

    public RecyclerAdapter(Activity activity, List<ListData> dataList, int type) {
        this.activity = activity;
        this.dataList = dataList;
        this.type = type;
    }

    @NonNull
    @Override
    public CommonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CommonViewHolder(parent, R.layout.item_view) {
            @Override
            public void onItemSelected(int position) {

            }
        };
    }

    /**
     * If type is 2 then set the width to the 65 to 75% of the screen.
     *
     * If type is 1 then don't alter the width of the main view.
     * */
    @Override
    public void onBindViewHolder(@NonNull CommonViewHolder holder, int position) {
        View view = holder.getView();
        ImageView imageView = view.findViewById(R.id.image_view);
        TextView textView = view.findViewById(R.id.content_value);
        RelativeLayout mainLayout = view.findViewById(R.id.main_view);

        if(type == 2){
            Display display = activity.getWindowManager().getDefaultDisplay();
            int width = display.getWidth();
            int newWidth = (width/2)+(width/4);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(newWidth, 350);
            mainLayout.setLayoutParams(layoutParams);
            mainLayout.setPadding(10,0,10,0);
        }

        Picasso.get().load(dataList.get(position).getImageUrl()).into(imageView);
        textView.setText(dataList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}