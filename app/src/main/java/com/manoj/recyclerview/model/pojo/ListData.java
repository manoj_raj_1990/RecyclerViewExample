package com.manoj.recyclerview.model.pojo;

public class ListData {
    String name;
    String imageUrl;

    public ListData(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
