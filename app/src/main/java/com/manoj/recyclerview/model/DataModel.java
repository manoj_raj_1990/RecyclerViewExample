package com.manoj.recyclerview.model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.manoj.recyclerview.model.pojo.ListData;

import java.util.ArrayList;
import java.util.List;

public class DataModel extends AndroidViewModel {

    public MutableLiveData<List<ListData>> listData = new MutableLiveData<>();

    public DataModel(@NonNull Application application) {
        super(application);
        lodeData();
    }

    private void lodeData() {
        List<ListData> dataList = new ArrayList<>();
        int i=0;
        while (i<10){
            if(i%2 == 0){
                dataList.add(new ListData("List Name "+i,"https://images5.alphacoders.com/933/933276.jpg"));
            }else {
                dataList.add(new ListData("List Name "+i,"https://images3.alphacoders.com/853/85305.jpg"));
            }
            i++;
        }
        listData.setValue(dataList);
    }

    public List<ListData> getData(){
        return listData.getValue();
    }

    /**
     * Default image URL for new added data
     * URL = https://images4.alphacoders.com/932/932271.jpg
     * */
    public void addNewData(String name){
        String imageLink = "https://images4.alphacoders.com/932/932271.jpg";
        ListData newData = new ListData(name, imageLink);
        List<ListData> dataList = new ArrayList<>();
        dataList.add(newData);
        listData.postValue(dataList);
    }

}